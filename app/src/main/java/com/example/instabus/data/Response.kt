package com.example.instabus.data

import com.squareup.moshi.Json

data class Response (
    @field:Json(name = "data") val data: NearStations
)