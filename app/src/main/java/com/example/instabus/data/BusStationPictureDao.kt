package com.example.instabus.data

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface BusStationPictureDao {

    @Query("SELECT * FROM busStationPictures WHERE busStationId = :busStationId")
    fun getAll(busStationId: Int): MutableList<BusStationPicture>

    @Insert
    suspend fun insertBusStationPicture(busStationPicture: BusStationPicture): Long

    @Query("DELETE FROM busStationPictures WHERE id = :busStationPictureId")
    suspend fun deleteBusStationPicture(busStationPictureId: Int)
}