package com.example.instabus.data

import com.squareup.moshi.Json

data class NearStations (
    @field:Json(name = "nearstations") val stationList: List<BusStation>
)