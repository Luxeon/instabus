package com.example.instabus.data

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface BusStationDao {

    @Query("SELECT * FROM busStations ORDER BY name")
    fun getAll(): List<BusStation>

    @Insert
    suspend fun insertBusStations(busStations: List<BusStation>)

    @Query("DELETE FROM busStations")
    suspend fun deleteAll()
}