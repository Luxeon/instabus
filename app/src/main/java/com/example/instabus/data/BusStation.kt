package com.example.instabus.data

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.Json

@Entity(tableName = "busStations")
data class BusStation (
    @PrimaryKey(autoGenerate = false)
    @field:Json(name = "id") val id: Int,
    @field:Json(name = "street_name") val name: String,
    @field:Json(name = "lat") val latitude: String,
    @field:Json(name = "lon") val longitude: String
)