package com.example.instabus.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [BusStation::class, BusStationPicture::class], version = 1, exportSchema = false)
abstract class BusStationDatabase: RoomDatabase() {

    abstract fun busStationDao(): BusStationDao
    abstract fun busStationPictureDao(): BusStationPictureDao

    companion object {
        @Volatile
        private var INSTANCE: BusStationDatabase? = null

        fun getDatabase(context: Context): BusStationDatabase {
            if (INSTANCE == null) {
                synchronized(this) {
                    INSTANCE = Room.databaseBuilder(context.applicationContext, BusStationDatabase::class.java, "bus_stations.db").build()
                }
            }
            return INSTANCE!!
        }
    }
}