package com.example.instabus.data

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "busStationPictures")
data class BusStationPicture (
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    val busStationId: Int,
    val title: String,
    val creationDate: String,
    val image: ByteArray
)