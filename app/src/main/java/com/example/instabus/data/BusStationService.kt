package com.example.instabus.data

import retrofit2.Response
import retrofit2.http.GET

interface BusStationService {
    @GET("/bus/nearstation/latlon/41.3985182/2.1917991/1.json")
    suspend fun getBusStationData(): Response<com.example.instabus.data.Response>
}