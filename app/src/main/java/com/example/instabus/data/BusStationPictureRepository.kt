package com.example.instabus.data

import android.app.Application
import androidx.annotation.WorkerThread
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class BusStationPictureRepository(val app: Application) {

    val busStationPictureData = MutableLiveData<MutableList<BusStationPicture>>()
    private val busStationPictureDao = BusStationDatabase.getDatabase(app).busStationPictureDao()

    @WorkerThread
    suspend fun insertOne(busStationPicture: BusStationPicture) {
        busStationPictureDao.insertBusStationPicture(busStationPicture)
    }

    @WorkerThread
    suspend fun deleteOne(busStationPicture: BusStationPicture) {
        busStationPictureDao.deleteBusStationPicture(busStationPicture.id)
    }

    fun refreshPictures(busStationId: Int) {
        CoroutineScope(Dispatchers.IO).launch {
            val data = busStationPictureDao.getAll(busStationId)
            busStationPictureData.postValue(data)
        }
    }
}