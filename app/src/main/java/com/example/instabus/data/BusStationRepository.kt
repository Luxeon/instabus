package com.example.instabus.data

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import android.util.Log
import androidx.annotation.WorkerThread
import androidx.lifecycle.MutableLiveData
import com.example.instabus.WEBSERVICE_URL
import com.example.instabus.LOG_TAG
import com.example.instabus.utils.FileHelper
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import com.squareup.moshi.Moshi




class BusStationRepository(val app: Application) {

    val busStationData = MutableLiveData<List<BusStation>>()
    private val busStationDao = BusStationDatabase.getDatabase(app).busStationDao()

    init {
        CoroutineScope(Dispatchers.IO).launch {
            val data = busStationDao.getAll()
            if (data.isEmpty()) {
                callWebService()
            }
            else {
                busStationData.postValue(data)
            }
        }
    }

    @WorkerThread
    suspend fun callWebService() {
        // API call deactivated because of not maintained API that return HTML in addition to the
        // JSON data and make the app crash

        /*if (networkAvailable()) {
            val retrofit = Retrofit.Builder()
                .baseUrl(WEBSERVICE_URL)
                .addConverterFactory(MoshiConverterFactory.create())
                .build()
            val service = retrofit.create(BusStationService::class.java)
            val serviceData = service.getBusStationData().body() ?: Response(data = NearStations(stationList = emptyList()))
            val busStations = serviceData.data.stationList
            val sortedBusStation = busStations.sortedWith(compareBy{it.name})
            busStationData.postValue(sortedBusStation)
            busStationDao.deleteAll()
            busStationDao.insertBusStations(serviceData.data.stationList)
        }*/

        // Instead, use of a clean registered JSON response saved in file

        val json = FileHelper.getTextFromAssets(app, "bus_station_data.json")

        val moshi = Moshi.Builder().build()
        val jsonAdapter = moshi.adapter(Response::class.java)

        val serviceData = jsonAdapter.fromJson(json) ?: Response(data = NearStations(stationList = emptyList()))
        val busStations = serviceData.data.stationList
        val sortedBusStation = busStations.sortedWith(compareBy{it.name})
        busStationData.postValue(sortedBusStation)
        busStationDao.deleteAll()
        busStationDao.insertBusStations(serviceData.data.stationList)
    }

    private fun networkAvailable(): Boolean {
        val connectivityManager = app.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connectivityManager.activeNetworkInfo
        return networkInfo?.isConnectedOrConnecting ?: false
    }

    fun refreshData() {
        CoroutineScope(Dispatchers.IO).launch {
            callWebService()
        }
    }
}