package com.example.instabus.ui.shared

import android.app.Application
import android.graphics.Bitmap
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.example.instabus.LOG_TAG
import com.example.instabus.data.BusStation
import com.example.instabus.data.BusStationPictureRepository
import com.example.instabus.data.BusStationRepository

class SharedViewModel(app: Application) : AndroidViewModel(app) {

    private val dataRepo = BusStationRepository(app)
    val busStationData = dataRepo.busStationData
    val selectedBusStation = MutableLiveData<BusStation>()

    val image = MutableLiveData<Bitmap>()

    private val pictureDataRepo = BusStationPictureRepository(app)
    val busStationPictureData = pictureDataRepo.busStationPictureData

    fun refreshData() {
        dataRepo.refreshData()
    }

    fun refreshPictures(busStationId: Int) {
        pictureDataRepo.refreshPictures(busStationId)
        Log.i(LOG_TAG, "Refreshing...")
    }
}
