package com.example.instabus.ui.main

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.instabus.R
import com.example.instabus.data.BusStation

class MainRecyclerAdapter(val context: Context, val busStations: List<BusStation>, val itemListener: BusStationItemListener): RecyclerView.Adapter<MainRecyclerAdapter.ViewHolder>() {

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val nameText = itemView.findViewById<TextView>(R.id.txt_name)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.bus_station_item, parent, false)

        return ViewHolder(view)
    }

    override fun getItemCount() = busStations.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val busStation = busStations[position]
        with(holder) {
            nameText?.let {
                it.text = busStation.name
                it.contentDescription = busStation.name
            }
            holder.itemView.setOnClickListener {
                itemListener.onBusStationItemClick(busStation)
            }
        }
    }

    interface BusStationItemListener {
        fun onBusStationItemClick(busStation: BusStation)
    }
}