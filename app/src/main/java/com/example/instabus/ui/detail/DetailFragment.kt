package com.example.instabus.ui.detail

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.example.instabus.CAMERA_REQUEST_CODE
import com.example.instabus.LOG_TAG
import com.example.instabus.R
import com.example.instabus.data.BusStationPictureRepository
import com.example.instabus.databinding.FragmentDetailBinding
import com.example.instabus.ui.shared.SharedViewModel
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


/**
 * A simple [Fragment] subclass.
 */
class DetailFragment : Fragment(), SwipeHelper.SwipeHelperListener {

    private lateinit var viewModel: SharedViewModel
    private lateinit var navController: NavController
    private lateinit var recyclerView: RecyclerView
    private lateinit var busStationPictureRepo : BusStationPictureRepository

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        busStationPictureRepo = BusStationPictureRepository(activity!!.application)

        (requireActivity() as AppCompatActivity).run {
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
        }
        setHasOptionsMenu(true)

        navController = Navigation.findNavController(requireActivity(), R.id.nav_fragment)

        viewModel = ViewModelProviders.of(requireActivity()).get(SharedViewModel::class.java)

        viewModel.refreshPictures(viewModel.selectedBusStation.value?.id ?: 0)

        val binding = FragmentDetailBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel

        val view = binding.root

        recyclerView = view.findViewById(R.id.detailRecyclerView)

        viewModel.busStationPictureData.observe(this.viewLifecycleOwner, Observer {
            val adapter = DetailRecyclerAdapter(requireContext(), it)
            recyclerView.adapter = adapter
        })

        val itemTouchHelperCallback = SwipeHelper(0, ItemTouchHelper.LEFT, this)
        ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(recyclerView)

        val addButton = view.findViewById<FloatingActionButton>(R.id.btn_add_photo)
        addButton.setOnClickListener {
            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            startActivityForResult(intent, CAMERA_REQUEST_CODE)
        }

        return view
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                navController.navigateUp()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == CAMERA_REQUEST_CODE) {
            var image = data?.extras?.get("data") as Bitmap
            viewModel.image.value = image
            navController.navigate(R.id.action_detailFragment_to_addPhotoFragment)
        }
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int, position: Int) {
        if (viewHolder is DetailRecyclerAdapter.ViewHolder) {
            val deletedIndex = viewHolder.adapterPosition

            val title = viewModel.busStationPictureData.value?.get(deletedIndex)?.title

            val deletedItem = viewModel.busStationPictureData.value!!.get(deletedIndex)

            (recyclerView.adapter as DetailRecyclerAdapter).removeRow(deletedIndex)

            val snackbar = Snackbar.make(requireView(), "$title removed", Snackbar.LENGTH_LONG)
            snackbar.setAction("UNDO") {
                (recyclerView.adapter as DetailRecyclerAdapter).restoreRow(deletedItem, deletedIndex)
            }
            snackbar.addCallback(object : Snackbar.Callback() {
                override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
                    super.onDismissed(transientBottomBar, event)
                    if (event == DISMISS_EVENT_TIMEOUT) {
                        CoroutineScope(Dispatchers.IO).launch {
                            busStationPictureRepo.deleteOne(deletedItem)
                        }
                    }
                }
            })
            snackbar.setActionTextColor(Color.YELLOW)
            snackbar.show()
        }
    }
}
