package com.example.instabus.ui.main

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

class ViewPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        when (position) {
            0 -> return ListFragment() //ChildFragment1 at position 0
            1 -> return MapFragment() //ChildFragment2 at position 1
        }

        return ListFragment()
    }

    override fun getCount(): Int {
        return 2 //three fragments
    }

    override fun getPageTitle(position: Int): CharSequence? {
        when (position) {
            0 -> return "List" //ChildFragment1 at position 0
            1 -> return "Map" //ChildFragment2 at position 1
        }

        return "List"
    }
}