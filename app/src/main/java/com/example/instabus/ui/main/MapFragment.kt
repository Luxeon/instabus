package com.example.instabus.ui.main

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.example.instabus.R
import com.example.instabus.data.BusStation
import com.example.instabus.ui.shared.SharedViewModel
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions

/**
 * A simple [Fragment] subclass.
 */
class MapFragment : Fragment(), OnMapReadyCallback {

    private lateinit var navController: NavController
    private lateinit var googleMap: GoogleMap
    private lateinit var viewModel: SharedViewModel
    private var mapInitialized = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        setHasOptionsMenu(true)

        navController = Navigation.findNavController(requireActivity(), R.id.nav_fragment)
        viewModel = ViewModelProviders.of(requireActivity()).get(SharedViewModel::class.java)

        val view = inflater.inflate(R.layout.fragment_map, container, false)

        val mapView = view.findViewById<MapView>(R.id.mv_barcelone)
        mapView.onCreate(savedInstanceState)
        mapView.onResume()

        mapView.getMapAsync(this)

        viewModel.busStationData.observe(this.viewLifecycleOwner, Observer {
            if (mapInitialized) {
                refreshMarkers()
            }
        })

        return view
    }

    override fun onMapReady(map: GoogleMap?) {
        map?.let {
            googleMap = it

            mapInitialized = true

            val barcelonePos = LatLng(41.3985182, 2.1917991)
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(barcelonePos, 14.toFloat()))

            googleMap.setOnMarkerClickListener {
                Log.i("aze", "${(it.tag as BusStation).name}")
                viewModel.selectedBusStation.value = it.tag as BusStation
                navController.navigate(R.id.action_mainFragment_to_detailFragment)

                false
            }

            refreshMarkers()
        }


    }

    private fun refreshMarkers() {
        googleMap.clear()

        viewModel.busStationData.value?.forEach {
            val markerPos = LatLng(it.latitude.toDouble(), it.longitude.toDouble())
            val marker = googleMap.addMarker(MarkerOptions().position(markerPos).title(it.name))
            marker.tag = it
        }
    }
}