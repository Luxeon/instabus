package com.example.instabus.ui.add

import android.graphics.Bitmap
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.example.instabus.R
import com.example.instabus.data.BusStationPicture
import com.example.instabus.data.BusStationPictureRepository
import com.example.instabus.ui.shared.SharedViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.ByteArrayOutputStream
import java.text.DateFormat
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class AddPhotoFragment : Fragment() {

    private lateinit var viewModel: SharedViewModel
    private lateinit var navController: NavController
    private lateinit var busStationPictureRepo : BusStationPictureRepository

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        (requireActivity() as AppCompatActivity).run {
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
        }
        setHasOptionsMenu(true)
        navController = Navigation.findNavController(requireActivity(), R.id.nav_fragment)

        viewModel = ViewModelProviders.of(requireActivity()).get(SharedViewModel::class.java)

        busStationPictureRepo = BusStationPictureRepository(activity!!.application)

        val view = inflater.inflate(R.layout.fragment_add_photo, container, false)

        view.findViewById<ImageView>(R.id.img_preview).setImageBitmap(viewModel.image.value)

        val validateButton = view.findViewById<Button>(R.id.btn_validate)
        validateButton.setOnClickListener {
            val busStationId = viewModel.selectedBusStation.value?.id ?: 0
            val title = view.findViewById<EditText>(R.id.edit_text_title).text.toString()
            val creationDate = DateFormat.getDateTimeInstance().format(Calendar.getInstance().time)
            val image = viewModel.image.value ?: Bitmap.createBitmap(0, 0, Bitmap.Config.ARGB_8888)
            val stream = ByteArrayOutputStream()
            image.compress(Bitmap.CompressFormat.PNG, 90, stream)
            val imageByteArray = stream.toByteArray()
            val busStationPicture = BusStationPicture(0, busStationId, title, creationDate, imageByteArray)
            CoroutineScope(Dispatchers.IO).launch {
                busStationPictureRepo.insertOne(busStationPicture)
                navController.navigateUp()
            }
        }

        return view
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                navController.navigateUp()
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
