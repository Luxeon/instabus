package com.example.instabus.ui.main

import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.viewpager.widget.ViewPager
import com.example.instabus.R
import com.google.android.material.tabs.TabLayout

class MainFragment : Fragment() {

    private lateinit var pagerAdapter: ViewPagerAdapter
    private lateinit var navController: NavController

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        (requireActivity() as AppCompatActivity).run {
            supportActionBar?.setDisplayHomeAsUpEnabled(false)
        }
        setHasOptionsMenu(true)

        navController = Navigation.findNavController(requireActivity(), R.id.nav_fragment)

        pagerAdapter = ViewPagerAdapter(childFragmentManager)

        val view = inflater.inflate(R.layout.main_fragment, container, false)

        val viewPager = view.findViewById<ViewPager>(R.id.vp_list_map)
        viewPager.adapter = pagerAdapter

        val tabLayout = view.findViewById<TabLayout>(R.id.tl_list_map)
        tabLayout.setupWithViewPager(viewPager)

        return view
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_corner, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_home -> {
                val tabLayout = activity?.findViewById<TabLayout>(R.id.tl_list_map)
                tabLayout?.getTabAt(0)?.select()
            }
            R.id.action_map -> {
                val tabLayout = activity?.findViewById<TabLayout>(R.id.tl_list_map)
                tabLayout?.getTabAt(1)?.select()
            }
            R.id.action_settings -> {
                navController.navigate(R.id.settingsActivity)
            }
        }
        return true
    }
}
