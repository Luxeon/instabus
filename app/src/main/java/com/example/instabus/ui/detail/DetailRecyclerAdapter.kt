package com.example.instabus.ui.detail

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.instabus.R
import com.example.instabus.data.BusStationPicture

class DetailRecyclerAdapter(val context: Context, val busStationPictures: MutableList<BusStationPicture>): RecyclerView.Adapter<DetailRecyclerAdapter.ViewHolder>() {

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val pictureTitle = itemView.findViewById<TextView>(R.id.txt_picture_title)
        val picture = itemView.findViewById<ImageView>(R.id.img_picture)
        val pictureCreationDate = itemView.findViewById<TextView>(R.id.txt_picture_creation_date)

        val pictureItemBackground = itemView.findViewById<ConstraintLayout>(R.id.background);
        val pictureItemForeground = itemView.findViewById<ConstraintLayout>(R.id.foreground);
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.bus_station_picture_item, parent, false)

        return ViewHolder(view)
    }

    override fun getItemCount() = busStationPictures.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val busStationPicture = busStationPictures[position]
        with(holder) {
            pictureTitle?.let {
                it.text = busStationPicture.title
            }
            pictureCreationDate?.let {
                it.text = busStationPicture.creationDate
            }
            Glide.with(context).load(busStationPicture.image).into(picture)
        }
    }

    fun removeRow(position: Int) {
        busStationPictures.removeAt(position)
        notifyItemRemoved(position)
    }

    fun restoreRow(busStationPicture: BusStationPicture, position: Int) {
        busStationPictures.add(position, busStationPicture)
        notifyItemInserted(position)
    }
}