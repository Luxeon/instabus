package com.example.instabus.utils

import android.content.Context

class FileHelper {

    companion object {
        fun getTextFromAssets(context: Context, filename: String): String {
            return context.assets.open(filename).use {
                it.bufferedReader().use {
                    it.readText()
                }
            }
        }
    }
}