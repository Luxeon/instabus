Instabus is an Android app for sharing photos at Barcelona bus stops.

This is an individual project carried out in **Kotlin** during my 3rd year of study. You can see a video presentation on my portfolio just right [here][1], in the Instabus section.

How to use it ?
---------------

* Once the application is launched, you can choose a bus stop either from the list or from the map of Barcelona
* For each bus stops, you can see the pictures taken, delete them or take another one.

Installation
------------

It is possible for you to test the application by yourself locally. For this, the source files are at your disposal.

Troubleshooting
---------------

* Make sure you have selected a SDK for your project in Android Studio. Version 1.8 work well.
* Make sure the *Android SDK Comman-line Tools* are installed (`Tools > SDK Manager > SDK Tools`).

About me
--------

I am a Computer Science Master student. You can find some of my other projects on my [Portfolio][1]. 

[1]: https://portfolio.cyrilpiejougeac.dev
